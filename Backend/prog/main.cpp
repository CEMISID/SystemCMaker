/*********************************************************************
AUTOR:Oscar Daniel Albornoz Mora
EMAIL:oalbornoz08@gmail.com
ORGANIZACION: Universidad de Los Andes (ULA)
ASIGNATURA:
ARCHIVO:
LENGUAJE:
REQUERIMIENTOS:
DESCRIPCION:
**********************************************************************/

#include <Module.h>
#include <Printer.h>
#include <ctime>

using std::cout;
using std::endl;

int main()
{

  clock_t begin = clock();
                               //Type    Name      Inputs        Ouputs
  Module * xor1 = new Module("Xor_Gate","xor1",{"a_in","b_in"},{"c_out"});
  Module * and1 = new Module("And_Gate","and1",{"a_in","b_in"},{"c_out"});

  /**Half Adder*/

  std::vector<Module*> subModHA {and1,xor1}; //Submodules

  Signal sgHA_1("A_in",{"xor1::a_in","and1::a_in"});
  Signal sgHA_2("B_in",{"xor1::b_in","and1::b_in"});
  Signal sgHA_3("xor1::c_out",{"S_out"});
  Signal sgHA_4("and1::c_out",{"C_out"});

  std::vector<Signal> signalHA {sgHA_1,sgHA_2,sgHA_3,sgHA_4}; //Signals

  Module * halfadder1 = new Module("HalfAdder","halfadder1",{"A_in","B_in"},{"C_out","S_out"},signalHA,subModHA);

  /**Full Adder*/

  Module * xor2 = new Module("Xor_Gate","xor2",{"a_in","b_in"},{"c_out"});
  Module * and2 = new Module("And_Gate","and2",{"a_in","b_in"},{"c_out"});
  Module * or1 = new Module("Or_Gate","or1",{"a_in","b_in"},{"c_out"});

  std::vector<Module*> subModFA {and1,and2,xor1,xor2,or1};

  Signal sgFA_1("A_in",{"xor1::a_in","and2::a_in"});
  Signal sgFA_2("B_in",{"xor1::b_in","and2::b_in"});
  Signal sgFA_3("C_in",{"xor2::b_in","and1::b_in"});

  Signal sgFA_4("sg_1","xor1::c_out",{"xor2::a_in","and1::a_in"});

  Signal sgFA_5("sg_2","and1::c_out",{"or1::a_in"});
  Signal sgFA_6("sg_3","and2::c_out",{"or1::b_in"});

  Signal sgFA_7("xor2::c_out",{"S_out"});
  Signal sgFA_8("or1::c_out",{"C_out"});

  std::vector<Signal> signalFA{sgFA_1,sgFA_2,sgFA_3,sgFA_4,sgFA_5,sgFA_6,sgFA_7,sgFA_8};

  Module * fulladder1 = new
    Module("FullAdder","fulladder1",{"A_in","B_in","C_in"},{"C_out","S_out"},signalFA,subModFA);


  /** 4 BIT ADDER */

  Module * fulladder2 = new Module ("fulladder2",fulladder1);
  Module * fulladder3 = new Module ("fulladder3",fulladder1);

  std::vector<Module*> subModFBA {halfadder1,fulladder1,fulladder2,fulladder3};

  Signal sgFBA_1("sg_1","halfadder1::C_out",{"fulladder1::C_in"});
  Signal sgFBA_2("sg_2","fulladder1::C_out",{"fulladder2::C_in"});
  Signal sgFBA_3("sg_3","fulladder2::C_out",{"fulladder3::C_in"});

  Signal sgFBA_null("sg_null","fulladder3::C_out",{});

  Signal sgFBA_4("X1_in",{"halfadder1::A_in"});
  Signal sgFBA_5("Y1_in",{"halfadder1::B_in"});
  Signal sgFBA_6("X2_in",{"fulladder1::A_in"});
  Signal sgFBA_7("Y2_in",{"fulladder1::B_in"});
  Signal sgFBA_8("X3_in",{"fulladder2::A_in"});
  Signal sgFBA_9("Y3_in",{"fulladder2::B_in"});
  Signal sgFBA_10("X4_in",{"fulladder3::A_in"});
  Signal sgFBA_11("Y4_in",{"fulladder3::B_in"});

  Signal sgFBA_12("halfadder1::S_out",{"Z1_out"});
  Signal sgFBA_13("fulladder1::S_out",{"Z2_out"});
  Signal sgFBA_14("fulladder2::S_out",{"Z3_out"});
  Signal sgFBA_15("fulladder3::S_out",{"Z4_out"});

  std::vector<Signal> signalFBA{sgFBA_1,sgFBA_2,sgFBA_3,sgFBA_4,sgFBA_5,sgFBA_6,sgFBA_7,sgFBA_8,
  	sgFBA_9,sgFBA_10,sgFBA_11,sgFBA_12,sgFBA_13,sgFBA_14,sgFBA_15,sgFBA_null};

  Module * fourbitadder1 = new Module("FourBitsAdder","fourbitsadder1",
  	{"X1_in","X2_in","X3_in","X4_in","Y1_in","Y2_in","Y3_in","Y4_in"},
    {"Z1_out","Z2_out","Z3_out","Z4_out"},signalFBA,subModFBA);


  Printer myPrinter;
  myPrinter.printAll(fourbitadder1);

  clock_t end = clock();
  double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;

  cout << endl << "Files generated in \"sample\" folder! (Check current folder)" <<endl;
  cout<<endl<<"in Time: "<< elapsed_secs << " seconds" <<endl<<endl;

/*  delete and1;
  delete xor1;
  delete and2;
  delete xor2;
  delete or1;
  delete halfadder1;
  delete fulladder1;
  delete fulladder2;
  delete fulladder3;
  delete fourbitadder1;

*/

  return 0;
}
