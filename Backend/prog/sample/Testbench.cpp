#include <Testbench.h>

Testbench::Testbench(sc_module_name nm):sc_module(nm)
{
	SC_THREAD(test);
	sensitive<<clk_in.neg();
	dont_initialize();
}

Testbench::~Testbench(){}

void Testbench::test()
{
	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(0);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(0);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(0);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(0);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(0);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(0);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(0);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(0);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

	X1_in.write(1);
	X2_in.write(1);
	X3_in.write(1);
	X4_in.write(1);
	Y1_in.write(1);
	Y2_in.write(1);
	Y3_in.write(1);
	Y4_in.write(1);

	wait();
	print();

sc_stop();
}

void Testbench::print()
{
	cout<<sc_time_stamp() << "  " << Y4_in.read() << Y3_in.read() << Y2_in.read() << Y1_in.read() << X4_in.read() << X3_in.read() << X2_in.read() << X1_in.read()<<"  "
	 << Z4_out.read() << Z3_out.read() << Z2_out.read() << Z1_out.read() << endl;
}