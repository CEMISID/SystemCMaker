#ifndef H_FullAdder
#define H_FullAdder
#include <systemc.h>
#include <And_Gate.h>
#include <Xor_Gate.h>
#include <Or_Gate.h>

class FullAdder : public sc_module
{
	public: 
		sc_in<bool> A_in;
		sc_in<bool> B_in;
		sc_in<bool> C_in;
		sc_out<bool> C_out;
		sc_out<bool> S_out;

		SC_CTOR(FullAdder);
		~FullAdder();
	private:
		And_Gate  * and1;
		And_Gate  * and2;
		Xor_Gate  * xor1;
		Xor_Gate  * xor2;
		Or_Gate  * or1;
		sc_signal<bool> sg_1;
		sc_signal<bool> sg_2;
		sc_signal<bool> sg_3;
};

#endif