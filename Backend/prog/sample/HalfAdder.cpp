#include <HalfAdder.h>

HalfAdder::HalfAdder(sc_module_name nm):sc_module(nm)
{
	and1 = new And_Gate("and1");
	xor1 = new Xor_Gate("xor1");

	and1->a_in(A_in);
	and1->b_in(B_in);
	and1->c_out(C_out);

	xor1->a_in(A_in);
	xor1->b_in(B_in);
	xor1->c_out(S_out);

}

HalfAdder::~HalfAdder()
{
	delete and1;
	delete xor1;
}
