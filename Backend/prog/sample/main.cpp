#include <FourBitsAdder.h>
#include <Testbench.h>

int sc_main(int argv, char* argc[])
{
	sc_time PERIOD(10,SC_NS);
	sc_time DELAY(10,SC_NS);
	sc_clock clock("clock", PERIOD, 0.5, DELAY, true);

	FourBitsAdder mod("mod");
	Testbench tb("tb");

	sc_signal<bool> X1_in;
	sc_signal<bool> X2_in;
	sc_signal<bool> X3_in;
	sc_signal<bool> X4_in;
	sc_signal<bool> Y1_in;
	sc_signal<bool> Y2_in;
	sc_signal<bool> Y3_in;
	sc_signal<bool> Y4_in;
	sc_signal<bool> Z1_out;
	sc_signal<bool> Z2_out;
	sc_signal<bool> Z3_out;
	sc_signal<bool> Z4_out;

	mod.X1_in(X1_in);
	mod.X2_in(X2_in);
	mod.X3_in(X3_in);
	mod.X4_in(X4_in);
	mod.Y1_in(Y1_in);
	mod.Y2_in(Y2_in);
	mod.Y3_in(Y3_in);
	mod.Y4_in(Y4_in);

	mod.Z1_out(Z1_out);
	mod.Z2_out(Z2_out);
	mod.Z3_out(Z3_out);
	mod.Z4_out(Z4_out);

	tb.X1_in(X1_in);
	tb.X2_in(X2_in);
	tb.X3_in(X3_in);
	tb.X4_in(X4_in);
	tb.Y1_in(Y1_in);
	tb.Y2_in(Y2_in);
	tb.Y3_in(Y3_in);
	tb.Y4_in(Y4_in);

	tb.clk_in(clock);

	tb.Z1_out(Z1_out);
	tb.Z2_out(Z2_out);
	tb.Z3_out(Z3_out);
	tb.Z4_out(Z4_out);

	sc_start();
	return 0;
}