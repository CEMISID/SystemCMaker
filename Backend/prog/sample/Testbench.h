#ifndef H_Testbench
#define H_Testbench
#include <systemc.h>

class Testbench : public sc_module
{
	public: 
		sc_in<bool> clk_in;
		sc_in<bool> Z1_out;
		sc_in<bool> Z2_out;
		sc_in<bool> Z3_out;
		sc_in<bool> Z4_out;
		sc_out<bool> X1_in;
		sc_out<bool> X2_in;
		sc_out<bool> X3_in;
		sc_out<bool> X4_in;
		sc_out<bool> Y1_in;
		sc_out<bool> Y2_in;
		sc_out<bool> Y3_in;
		sc_out<bool> Y4_in;

		SC_CTOR(Testbench);
		~Testbench();
	private:
		void test();
		void print();
};

#endif