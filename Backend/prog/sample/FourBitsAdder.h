#ifndef H_FourBitsAdder
#define H_FourBitsAdder
#include <systemc.h>
#include <HalfAdder.h>
#include <FullAdder.h>

class FourBitsAdder : public sc_module
{
	public: 
		sc_in<bool> X1_in;
		sc_in<bool> X2_in;
		sc_in<bool> X3_in;
		sc_in<bool> X4_in;
		sc_in<bool> Y1_in;
		sc_in<bool> Y2_in;
		sc_in<bool> Y3_in;
		sc_in<bool> Y4_in;
		sc_out<bool> Z1_out;
		sc_out<bool> Z2_out;
		sc_out<bool> Z3_out;
		sc_out<bool> Z4_out;

		SC_CTOR(FourBitsAdder);
		~FourBitsAdder();
	private:
		HalfAdder  * halfadder1;
		FullAdder  * fulladder1;
		FullAdder  * fulladder2;
		FullAdder  * fulladder3;
		sc_signal<bool> sg_1;
		sc_signal<bool> sg_2;
		sc_signal<bool> sg_3;
		sc_signal<bool> sg_null;
};

#endif