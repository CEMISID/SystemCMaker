#include <FullAdder.h>

FullAdder::FullAdder(sc_module_name nm):sc_module(nm)
{
	and1 = new And_Gate("and1");
	and2 = new And_Gate("and2");
	xor1 = new Xor_Gate("xor1");
	xor2 = new Xor_Gate("xor2");
	or1 = new Or_Gate("or1");

	and1->a_in(sg_1);
	and1->b_in(C_in);
	and1->c_out(sg_2);

	and2->a_in(A_in);
	and2->b_in(B_in);
	and2->c_out(sg_3);

	xor1->a_in(A_in);
	xor1->b_in(B_in);
	xor1->c_out(sg_1);

	xor2->a_in(sg_1);
	xor2->b_in(C_in);
	xor2->c_out(S_out);

	or1->a_in(sg_2);
	or1->b_in(sg_3);
	or1->c_out(C_out);

}

FullAdder::~FullAdder()
{
	delete and1;
	delete and2;
	delete xor1;
	delete xor2;
	delete or1;
}
