#include <FourBitsAdder.h>

FourBitsAdder::FourBitsAdder(sc_module_name nm):sc_module(nm)
{
	halfadder1 = new HalfAdder("halfadder1");
	fulladder1 = new FullAdder("fulladder1");
	fulladder2 = new FullAdder("fulladder2");
	fulladder3 = new FullAdder("fulladder3");

	halfadder1->A_in(X1_in);
	halfadder1->B_in(Y1_in);
	halfadder1->C_out(sg_1);
	halfadder1->S_out(Z1_out);

	fulladder1->A_in(X2_in);
	fulladder1->B_in(Y2_in);
	fulladder1->C_in(sg_1);
	fulladder1->C_out(sg_2);
	fulladder1->S_out(Z2_out);

	fulladder2->A_in(X3_in);
	fulladder2->B_in(Y3_in);
	fulladder2->C_in(sg_2);
	fulladder2->C_out(sg_3);
	fulladder2->S_out(Z3_out);

	fulladder3->A_in(X4_in);
	fulladder3->B_in(Y4_in);
	fulladder3->C_in(sg_3);
	fulladder3->C_out(sg_null);
	fulladder3->S_out(Z4_out);

}

FourBitsAdder::~FourBitsAdder()
{
	delete halfadder1;
	delete fulladder1;
	delete fulladder2;
	delete fulladder3;
}
