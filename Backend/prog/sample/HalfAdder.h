#ifndef H_HalfAdder
#define H_HalfAdder
#include <systemc.h>
#include <And_Gate.h>
#include <Xor_Gate.h>

class HalfAdder : public sc_module
{
	public: 
		sc_in<bool> A_in;
		sc_in<bool> B_in;
		sc_out<bool> C_out;
		sc_out<bool> S_out;

		SC_CTOR(HalfAdder);
		~HalfAdder();
	private:
		And_Gate  * and1;
		Xor_Gate  * xor1;
};

#endif