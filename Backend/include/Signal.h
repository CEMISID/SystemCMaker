#ifndef SIGNAL_H
#define SIGNAL_H value

using namespace std;

class Printer;

class Signal
{
private:
  string name;
  string leavesFrom;
  vector<string> arrivesTo;
public:
  Signal(){};
  Signal(const string & lF, const vector<string> & aT):
    name(""),leavesFrom(lF),arrivesTo(aT){};

  Signal(const string & n , const string & lF,const vector<string> & aT):
    name(n),leavesFrom(lF),arrivesTo(aT){};

  ~Signal(){};
  
   friend Printer;
};

#endif

