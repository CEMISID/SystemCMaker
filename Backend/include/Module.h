#ifndef MODULE_H
#define MODULE_H

#include <iostream>
#include <vector>
#include "Signal.h"

using namespace std;

class Printer;

class Module
{
  private:
    string type;
    string name;

    vector<string> namesInputs;
    vector<string> namesOutputs;
    vector<Signal> signals;

    vector<Module*> modules;

  public:
    Module(){};

    Module(const string &,const string &, vector<string>, 
      vector<string>);
    
    Module(const string &,const string &, vector<string>,
      vector<string>, vector<Signal>, vector<Module*>);

    Module(const string &, const Module *);

    ~Module();

    string getType() const;

    inline bool hasSubmodules() const { return ! modules.empty(); }

    Module operator = (const Module &m);
    
    friend Printer;

};


#endif