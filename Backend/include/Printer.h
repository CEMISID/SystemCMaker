#ifndef PRINTER_H
#define PRINTER_H

#include <fstream>
#include <vector>
#include <vector>
#include <math.h>
#include <sys/stat.h> 

using namespace std;

class Module;

class Printer
{
  private:
    vector<string> headerPrinted;
    vector<string> cppPrinted;

  public:
    Printer(){
        mkdir("sample",0777);
    };
    ~Printer(){};

    void printHeader(const Module *) ;
    void printCpp(const Module *);
    void printTestbench(const Module *);
    void printMain(const Module *);
    void printMakeFile(const Module *);
    void printAll(const Module *);
}; 

#endif