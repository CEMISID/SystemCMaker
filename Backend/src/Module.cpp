#include <Module.h>

using namespace std;

Module::Module(const string &t,const string &n, vector<string> nI,
  vector<string> nO) :
type(t),name(n),namesInputs(nI),namesOutputs(nO)
{
};

Module::Module(const string &t,const string &n, vector<string> nI,
  vector<string> nO,vector<Signal> s,vector<Module*> m) :
type(t),name(n),namesInputs(nI),namesOutputs(nO),signals(s),modules(m)
{
};

Module::Module(const string &name, const Module * m)
{
  this->name=name;

  this->type=m->type;
  this->namesInputs=m->namesInputs;
  this->namesOutputs=m->namesOutputs;
  this->signals=m->signals;
  this->modules=m->modules;
}


Module Module::operator = (const Module & m)
{
  this->type=m.type;
  this->name=m.name;
  this->namesInputs=m.namesInputs;
  this->namesOutputs=m.namesOutputs;
  this->signals=m.signals;
  this->modules=m.modules;

  return (*this);
};


Module::~Module()
{
  //cout << Module::eliminados << endl;
  for (auto & item : modules)
    delete item;
}

//! This should change

string Module::getType() const
{
  if (type == "And_Gate")
      return "and";
  if (type == "Or_Gate") 
      return "or";
  if (type ==  "Not_Gate") 
      return "not";
  if (type ==  "Xor_Gate") 
      return "xor";
  if (type ==  "Nand_Gate") 
      return "not and";
  if (type ==  "Nor_Gate") 
      return "not or";
  else
      return "";
};



