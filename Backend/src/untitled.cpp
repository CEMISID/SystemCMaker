for (auto currentSignal = currentModule->signals.begin();currentSignal != currentModule->signals.end() and not signalFound; ++currentSignal) 
      {	
        if (find(currentSignal->arrivesTo.begin(),currentSignal->arrivesTo.end(),subModule->name + "::" + variableName))
        {
          signal = currentSignal->name.empty() ? currentSignal->name : currentSignal->leavesFrom; 
          signalFound = true;
        }
      }