/*!*******************************************************************
AUTOR:Oscar Daniel Albornoz Mora
EMAIL:oalbornoz08@gmail.com
ORGANIZACION: Universidad de Los Andes (ULA)
ASIGNATURA:
ARCHIVO:	
LENGUAJE:
REQUERIMIENTOS:
DESCRIPCION:	
**********************************************************************/

#include <Printer.h>

#include <Module.h>

#include <iomanip>


inline string define(const string & name) 
{ 
  return "#ifndef H_" + name + '\n' +
         "#define H_" + name;
}

inline string include(const string & name) 
{
  return "#include <" + name + '>';
}

inline string declareClass(const string & name)
{
  return "class " + name;
}

inline string declareVariable(const string & type,const string & name)
{
  return type + ' ' + name;
}

inline string declareMethod(const string & name,const string & parameter = "")
{
  return name + '(' + parameter + ')';
}

inline string declareFunction(const string & type,const string & name,const string & parameter = "")
{
  return type + ' ' + name + '(' + parameter + ')';
}

string integerToBinary(int, const size_t &);

template<class InputIterator, class T>
bool find (InputIterator, InputIterator, const T &);


void Printer::printHeader(const Module * curModule)
{
  string currentClass = curModule->type; /*!< Get the type of class */

  //! Base case 
  /*! This allow to method does not reprint the same file */

  for(const auto & wasPrintedAlready : headerPrinted) 
    if (currentClass == wasPrintedAlready)
      return;

  //! If the current module has not submodules so it must be a Gate

  if ( not curModule->hasSubmodules()) 
  {
    ofstream file("sample/" + currentClass + ".h");
    if (file.fail())
      throw ("Error opening header file: " + currentClass );

    /*!
    #ifndef ###
    #define ###
    #include <systemc.h>
    class ### : public sc_module
    	public: 
    */

    file << define(currentClass) << '\n';
    file << include("systemc.h") << '\n' << '\n';
    file << declareClass(currentClass + " : public sc_module") << '\n';
    file << '{' << '\n';
    file << '\t' << "public: " << '\n';

    //! To declare all modules's inputs and ouputs
    /*!
    sc_in<bool>  ###;
    sc_in<bool>  ###;
    sc_out<bool> ###;
    */

    for(const auto & varName : curModule->namesInputs)
      file << "\t\t" << declareVariable("sc_in<bool>",varName) << ';' << '\n';

    for(const auto & varName : curModule->namesOutputs)
      file << "\t\t" << declareVariable("sc_out<bool>",varName) << ';'<<'\n';

    //! To declare constructor and destroyer
    /*!
    SC_CTOR(###);
    ~###();
    */

    file << '\n';
    file << "\t\t" << declareMethod("SC_CTOR",currentClass) << ';' << '\n';
    file << "\t\t" << declareMethod("~" + currentClass) << ';' << '\n';
    file << '\t' << "private:" << '\n';


    //! To declare methods
    /*!
    void ###();
    */

    file << "\t\t" << declareFunction("void","operation") << ';' << '\n';
    file << "};" << '\n';
    file << '\n'<< "#endif";
    file.close();

    headerPrinted.push_back(currentClass);

    return;

  }

  //! Recursion
  /*!
  call to method PrinterHeader for each submodule 
  */

  for(const auto & subModule : curModule->modules)
    printHeader(subModule);

  //! Module
  /*!
  if current module has internal submodules
  */

  ofstream file("sample/" + currentClass + ".h" );
  if (file.fail())
    throw ("Error opening header file: " + currentClass );


  //! To include the headers of all submodules
  /*!
  #ifndef ###
  #define ###
  #include <systemc.h>
  #include <###>
  #include <###>
  */

  file << define(currentClass) << '\n';
  file << include("systemc.h") << '\n';

  vector<string> headerIncluded; /*!< To know which headers were already included */

  for (const auto & subModule : curModule->modules)
  {
    if (find(headerIncluded.begin(),headerIncluded.end(),subModule->type))
      continue;

    file << include(subModule->type+".h") << '\n'; /*!< Including submodule's header */

    headerIncluded.push_back(subModule->type);
  }

  //! To declare all modules's inputs and ouputs
  /*!
  class ### : public sc_module
  public: 
  sc_in<bool>  ###;
  sc_in<bool>  ###;
  sc_out<bool> ###;

  SC_CTOR(###);
  ~###();
  */

  file << '\n' << declareClass(currentClass + " : public sc_module") << '\n';
  file << '{' << '\n';

  file << '\t' << "public: " << '\n';

  for(const auto & varName : curModule->namesInputs)
    file << "\t\t" << declareVariable("sc_in<bool>",varName) << ';' << '\n';

  for(const auto & varName : curModule->namesOutputs)
    file << "\t\t" << declareVariable("sc_out<bool>",varName) << ';' << '\n';

  file << '\n';
  file << "\t\t" << declareMethod("SC_CTOR",currentClass) << ';' << '\n';
  file << "\t\t" << declareMethod("~" + currentClass) << ';' << '\n';

  file << '\t' << "private:" << '\n';

  //! To declare all modules's submodules and signals
  /*!
  ### * ###;
  And_Gate * and1;
  ### * ###;

  sc_signal<bool> ###;
  sc_signal<bool> ###;
  */

  for(const auto & subModule : curModule->modules)
    file << "\t\t" << declareVariable(subModule->type," * " + subModule->name) << ';' << '\n';

  //! Only explicit signals are declared

  for(const auto & sg : curModule->signals)
  {
    if(sg.name not_eq "") 
      file << "\t\t" << declareVariable("sc_signal<bool>",sg.name) << ';' << '\n';
  }

  file << "};" << '\n';
  file << "\n#endif";
  file.close();

  headerPrinted.push_back(curModule->type);
  return;
};

void Printer::printCpp(const Module * curModule)
{
  string currentClass = curModule->type;

  //! Base case 
  /*! This allow to method does not reprint the same file */

  for(const auto & wasPrintedAlready : cppPrinted)
    if (currentClass == wasPrintedAlready)
      return;

  if (not curModule->hasSubmodules())
  {
    ofstream file("sample/" + currentClass +".cpp");
    if (file.fail())
      throw ("Error opening header file: " + currentClass );

    /*!
    include <###.h>

    ###::###(sc_module_name nm):sc_module(nm)
    SC_METHOD(operation);
    sensitive << ### << ###;
    */

    file << include(currentClass + ".h" ) << "\n\n";
    file << declareMethod(currentClass + "::" + currentClass,"sc_module_name nm"); 
    file << ":sc_module(nm)\n{" << '\n';

    file << '\t' << declareMethod("SC_METHOD","operation") << ';' << '\n';

    file << "\tsensitive";

    for(const auto & varName : curModule->namesInputs)
      file << " << " << varName;

    file << ";" << '\n';
    file << "};" << '\n';
    file << '\n';

    /*!
    ###::~###(){};
    */
  
    file << declareMethod(currentClass + "::~" + currentClass);
    file << "{};" << '\n';

    /*!
    void ###::operation()
    {
    ###.write(###.read() *** ###.read());
    }
    */

    file << '\n';
    file << declareFunction("void",currentClass + "::operation");
    file << '\n' << '{' << '\n';

    string logicOperation = curModule->getType(); /*!< To know the logic operation */

    file << '\t' << curModule->namesOutputs.front() << ".write(";

    //! Not Gate only has one input and one output

    if (currentClass == "Not_Gate")
      file << " " << logicOperation << " ";  

    file << curModule->namesInputs.front() << ".read()"; /*! Here */

    for(const auto & varName : curModule->namesInputs)
    {
      if (varName == curModule->namesInputs.front()) 
        continue; /*!< because that already was printed up */

      file << " " << logicOperation << " " << varName << ".read()";
    }

    file << ");\n}" << '\n';
    file.close();

    cppPrinted.push_back(currentClass);

    return;
  }

  //! Recursion
  /*!
  call to method PrinterCpp for each submodule 
  */

  for(const auto & subModule : curModule->modules)
    printCpp(subModule);

  ofstream file("sample/" + currentClass + ".cpp");
  if (file.fail())
    throw ("Error opening header file: " + currentClass );

  /*!
  include <###.h>

  ###::###(sc_module_name nm):sc_module(nm)

  ### = new ###("###");
  ### = new ###("###");
  */

  file << include(currentClass + ".h" ) << "\n\n";
  file << declareMethod(currentClass + "::" + currentClass,"sc_module_name nm"); 
  file << ":sc_module(nm)\n{" << '\n';


  for(const auto & subModule : curModule->modules)
    file << '\t' << subModule->name << " = " << declareFunction("new",subModule->type,"\""+
    subModule->name+"\"") << ';' << '\n';
  
  file << '\n';

  /*!
  ###->###_in (###_in/sg);
  ###->###_in (###_in/sg);
  ###->###_out (###_out/sg);

  ###->###_in (###_in/sg);
  ###->###_in (###_in/sg);
  ###->###_out (###_out/sg);

  */


  //! To print the connections
  /*!
  Here the method prints the modules's internal connections, so he must access and prints
  each port of its submodules, at the same time he should look for signals which are associated to those ports 

  Explicit signal : A signal that connect the submodule's out to 
  the other submodule's input is Explicit. She has a name so she must to be declared.

  Example: 
          
  halfadder1->C_out(sg_1)
  fulladder1->C_in(sg_1)

  Implicit signal : A signal that connect the module's input to its submodule's input or the its submodule's output 
  to module's output is implicit, she has not a name so she does not must to be declared

  Example: 
          
  halfadder1->A_in(X1_in) or fulladder3->S_out(Z4_out)

  */

  bool signalFound;
  string signal,port; /*!< Can be the name of a signal or name of modules's input/output */ 

  for(auto subModule : curModule->modules) 
  {
    for(auto varName : subModule->namesInputs) 
    {
      signal.clear();
      port = subModule->name + "::" + varName;
      signalFound = false; /*!< To know if the signals was found already and stop the look for */

      //! Looking for the signal associated to current port

      for (auto curSignal = curModule->signals.begin(); curSignal != curModule->signals.end() and not signalFound; ++curSignal)
      {
        if (find(curSignal->arrivesTo.begin(),curSignal->arrivesTo.end(),port))
        {
          signal = curSignal->name.empty() ? curSignal->leavesFrom : curSignal->name; 
          signalFound = true;
        }
      }

      file << '\t' << subModule->name << "->" << varName << '(' << signal << ')' << ';' << '\n';
    }

    for(const auto & varName : subModule->namesOutputs)
    { 
      signal.clear();
      port = subModule->name + "::" + varName;

      for (const auto & curSignal : curModule->signals)
      {	
        if ( curSignal.leavesFrom == port )
        {
          signal = curSignal.name.empty() ? curSignal.arrivesTo.back() : curSignal.name; /*!< use back() method because that container should has only one element */
          break;
        }
      }

      file << '\t' << subModule->name << "->" << varName << '(' << signal << ')' << ';' << '\n';
    }

    file << '\n';
  }

  file << "};" << '\n';

  /*!

  ###::~###()
  {;
  delete ###;
  delete ###
  };

  */

  file << '\n';
  file << declareMethod(currentClass + "::~" + currentClass);
  file << '\n' << '{' << '\n';

  for(const auto & subModule : curModule->modules)
    file << '\t' << "delete " << subModule->name << ';' << '\n';

  file << "};" << '\n';

  file.close();

  cppPrinted.push_back(curModule->type);

  return;
};

void Printer::printTestbench(const Module * curModule)
{
  string currentClass = "Testbench";

  ofstream file("sample/" + currentClass + ".h");
  if (file.fail())
    throw ("Error opening header file: " + currentClass );

  /*!
  #ifndef ###
  #define ###

  #include <systemc.h>

  class ### : public sc_module
  */

  file << define(currentClass) << '\n';
  file << include("systemc.h") << '\n' << '\n';
  file << declareClass(currentClass + " : public sc_module") << '\n';
  file << '{' << '\n';

  file << '\t' << "public: " << '\n';

  /*!
  sc_in<bool> clk_in;	
  sc_in<bool>  ###;
  sc_in<bool>  ###;
  sc_out<bool> ###;
  */

  file << "\t\t" << declareVariable("sc_in<bool>","clk_in") << ';' << '\n';

  for(const auto & varName : curModule->namesOutputs)
    file << "\t\t" << declareVariable("sc_in<bool>",varName) << ';' << '\n';

  for(const auto & varName : curModule->namesInputs)
    file << "\t\t" << declareVariable("sc_out<bool>",varName) << ';'<<'\n';

  /*!
  SC_CTOR(###);
  ~###();
  */

  file << '\n';
  file << "\t\t" << declareMethod("SC_CTOR",currentClass) << ';' << '\n';
  file << "\t\t" << declareMethod("~" + currentClass) << ';' << '\n';

  file << '\t' << "private:" << '\n';

  /*!
  void test();
  void print();
  */

  file << "\t\t" << declareFunction("void","test") << ';' << '\n';
  file << "\t\t" << declareFunction("void","print") << ';' << '\n';

  file << "};" << '\n';
  file << '\n' << "#endif";

  file.close();

  /*! PRINT CPP TESTBENCH*/

  file.open("sample/" + currentClass +".cpp");
  if (file.fail())
    throw ("Error opening header file: " + currentClass );

  /*!
  include <###.h>

  ###::###(sc_module_name nm):sc_module(nm)
  SC_METHOD(operation);
  sensitive << ### << ###;
  dont_initialize();
  */

  file << include(currentClass + ".h" ) << "\n\n";
  file << declareMethod(currentClass + "::" + currentClass,"sc_module_name nm"); 
  file << ":sc_module(nm)\n{" << '\n';

  file << '\t' << declareMethod("SC_THREAD","test") << ';' << '\n';

  file << '\t'<< "sensitive<<clk_in.neg()";
  file << ";" << '\n';

  file << '\t'<< "dont_initialize()" << ';' <<'\n';

  file << '}' << '\n';
  file << '\n';

  /*!
  ###::~###(){};
  */

  file << declareMethod(currentClass + "::~" + currentClass);
  file << "{}" << '\n' << '\n';

  /*!
  void Testbench::test()
  */

  file << declareFunction("void",currentClass + "::" + "test");
  file << '\n' << '{' << '\n';
  
  /*!
  ###_in.write(#);
	###_in.write(#);

	wait();
	print();

	*/

  string binaryNumber;
  int j;

  for (int i = 0; i < pow(2,curModule->namesInputs.size()); ++i)
  {
    binaryNumber = integerToBinary(i,curModule->namesInputs.size());
    
    j = curModule->namesInputs.size();

    for(const auto & varName : curModule->namesInputs)
      file << '\t' << varName <<".write(" << binaryNumber[--j] << ')' << ';' << '\n';
    
    file << '\n';
    file << '\t' << "wait();"<<'\n';
    file << '\t' << "print();"<<'\n';
    file << '\n';
  }

  file << "sc_stop()" << ';' << '\n';
  file << '}' << '\n' << '\n';

  /*!
  void Testbench::print()
  */

  file << declareFunction("void",currentClass + "::" + "print");
  file << '\n' << '{' << '\n';

  /*!
	cout<<sc_time_stamp() << ###_in.read() << ###_in.read() << ###_out.read() << endl;
  */

  file << "\tcout<<sc_time_stamp() << \"  \"";

  for (auto  i = curModule->namesInputs.rbegin(); i not_eq curModule->namesInputs.rend(); ++i)
  {
    file << " << " << *i << ".read()";
  }

  file << "<<\"  \"" << '\n' << '\t';

  for (auto i = curModule->namesOutputs.rbegin(); i not_eq curModule->namesOutputs.rend(); ++i)
  {
    file << " << " << *i << ".read()";
  }
    
  file << " << endl;" << '\n' ;
  file << '}';

  file.close();

  cppPrinted.push_back(currentClass);
  return;
}

void Printer::printMain(const Module * curModule)
{
  string currentClass = curModule->type;

  ofstream file("sample/main.cpp");
  if (file.fail())
    throw ("Error opening header file: main ");

  /*!
  #include <systemc.h>
  */

  file << include(currentClass + ".h") << '\n';
  file << include("Testbench.h") << '\n' << '\n';

  /*!
  int sc_main(int argv, char* argc[])
  {
    sc_time PERIOD(10,SC_NS);
    sc_time DELAY(10,SC_NS);  
    sc_clock clock("clock",PERIOD,0.5,DELAY,true);
  */

  file << declareFunction ("int","sc_main","int argv, char* argc[]") << '\n';
  file << '{' << '\n';
  file << '\t' << declareFunction("sc_time","PERIOD","10,SC_NS") << ';' << '\n';
  file << '\t' << declareFunction("sc_time","DELAY","10,SC_NS") << ';' << '\n';
  file << '\t' << declareFunction("sc_clock","clock","\"clock\",PERIOD,0.5,DELAY,true") << ';' << '\n';

  file << '\n';

  /*!
  ### mod("mod");
  Testbench tb("tb");
  */

  file << '\t' << declareFunction(currentClass,"mod","\"mod\"") << ';' << '\n';
  file << '\t' << declareFunction("Testbench","tb","\"tb\"") << ';' << '\n';
  file << '\n';

  for(const auto & varName : curModule->namesInputs)
    file << '\t' << declareVariable("sc_signal<bool>",varName) << ';' << '\n';

  for(const auto & varName : curModule->namesOutputs)
    file << '\t' << declareVariable("sc_signal<bool>",varName) << ';' << '\n';

  file << '\n';

  for(const auto & varName : curModule->namesInputs) 
  {
    file << '\t' << "mod" << '.' << varName << '(' << varName << ')' << ';' << '\n';
  }

  file << '\n';

  for(const auto & varName : curModule->namesOutputs) 
  {
    file << '\t' << "mod" << '.' << varName << '(' << varName << ')' << ';' << '\n';
  }

  file << '\n';

  for(const auto & varName : curModule->namesInputs) 
  {
    file << '\t' << "tb" << '.' << varName << '(' << varName << ')' << ';' << '\n';
  }

   /*! tb.clk_in(clock); */

  file << '\n';
  file << '\t' << "tb" << '.' << "clk_in" << '(' << "clock" << ')' << ';' << '\n';
  file << '\n';

  for(const auto & varName : curModule->namesOutputs) 
  {
    file << '\t' << "tb" << '.' << varName << '(' << varName << ')' << ';' << '\n';
  }

  file << '\n';
  file << '\t' << declareMethod("sc_start") << ';' <<'\n';
  file << '\t' << "return 0" << ';' << '\n';
  file << '}';

  cppPrinted.push_back("main");
}

void Printer::printMakeFile(const Module * curModule)
{
  ofstream file("sample/makefile");
  if (file.fail())
    throw ("Error opening header file: Makefile ");

  file << "SRCS = ";

  for (const auto & item: cppPrinted)
  {
    file << " " + item + ".cpp";
  }
  
  file << '\n';

  file << 
        "\nTARGET = test"
        "\nSYSTEMC = /usr/local/systemc-2.3.1"
        "\nINCDIR = -I. -I.. -I$(SYSTEMC)/include"
        "\nLIBDIR = -L. -L.. -L$(SYSTEMC)/lib-linux/"
        "\nLIBS   = -lsystemc -lm"
        "\n"
        "\nCC     = clang++"
        "\nCFLAGS = -g -Wno-deprecated -Wall"
        "\nOBJS   = $(SRCS:.cpp=.o)"
        "\n"
        "\nOTHER  = -Wl,-rpath=/$(SYSTEMC)/lib-linux -lsystemc"
        "\n"
        "\nEXE    = $(TARGET)"
        "\n"
        "\n .SUFFIXES: .cpp .o  "
        "\n"
        "\n$(EXE): $(OBJS) $(SRCH) " 
        "\n\t$(CC) $(CFLAGS) $(INCDIR) $(LIBDIR) $(OTHER) -o $@ $(OBJS) $(LIBS) 2>&1 | c++filt"
        "\n" 
        "\nall: $(EXE) exp"
        "\n"
        "\n.cpp.o:"
        "\n\t$(CC) $(DEBUG) $(CFLAGS) $(INCDIR) -c $<"
        "\n"
        "\nclean:"
        "\n\trm -f $(OBJS) *~ $(EXE) *.bak $(TARGET).stackdump *.vcd"
        "\n"
        "\nexp:"
        "\n\texport LD_LIBRARY_PATH=/home/usuarios/is/systemc/lib-linux"
        "\n\texport LD_LIBRARY_PATH=$(SYSTEMC)/lib-linux"<< '\n';

  file.close();
}

void Printer::printAll(const Module * curModule)
{
  printHeader(curModule);

  printCpp(curModule);

  printTestbench(curModule);

  printMain(curModule);

  printMakeFile(curModule);
}

string integerToBinary(int integerNumber,const size_t & size)
{ 
  string binaryNumber;
  int module;

  while(integerNumber not_eq 0)
  { 
    module = integerNumber % 2;
    integerNumber /= 2;
    binaryNumber.insert(0,to_string(module));
  }

  while (binaryNumber.size() < size)  /*! To fill string */
    binaryNumber.insert(0,"0"); 

  return binaryNumber;   
}

template<class InputIterator, class T>
bool find (InputIterator first, InputIterator last, const T & val)
{
  while (first not_eq last) 
  {
    if (*first == val) 
      return true;
    ++first;
  }
  return false;
}


