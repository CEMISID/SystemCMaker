Program to simulate circuits logics in graphic mode , friendly and easy to handle.

Software that makes it possible to simulate the behavior of the connections between gates and other logical devices before a given input.

The way the gates are connected is through drag elements on a canvas.

It allows to generate SystemC code from the schema realized.
With their respective testbench.

feactures:
	-panel of elements dragables (signals , modules , base gates , ect).
	-Interfaz	 to add new elements made by user.
	-Interfaz to generate SystemC code.

	... come soon new feactures.

For run this program you need Qt libraries (5.5) or major.

-qmake
-make

...