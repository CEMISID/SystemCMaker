#include "GraphicsModuleItem.h"
#include <QDebug>
#include <QGraphicsSceneMouseEvent>
#include <QPoint>
#include <QPainter>


GraphicsModuleItem::GraphicsModuleItem(QGraphicsItem *parent, Module *module):
  QGraphicsPixmapItem(parent),m_module(module)
{
  if (module->imgDir() != "NONE")
  {
    this->setPixmap(QPixmap(module->imgDir()));
    this->setPos(module->position());
  }
  else
  {
    this->setPixmap(QPixmap(module->height(),module->height()));
    this->pixmap().fill(Qt::black);
  }

  this->setFlags(QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable );

  createPinsIn();
  createPinsOut();
  connect(this,SIGNAL(positionModuleChanged(QPointF)),this,SLOT(positionChangedSlot(QPointF)));

}

GraphicsModuleItem::GraphicsModuleItem(QGraphicsItem *parent):
  QGraphicsPixmapItem(parent)

{
  m_module = new Module("none");
  this->setPixmap(QPixmap(m_module->height(),m_module->height()));
  this->setPos(0,0);
  this->setFlags(QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable );

}

int GraphicsModuleItem::setModuleItem(Module *module)
{
 if(module == nullptr)
   return 1;
 m_module =module;
 return 0;
}

void GraphicsModuleItem::setPositionGate(QPointF p)
{
  m_module->setPosition(p);
  emit(positionModuleChanged(p));
}

int GraphicsModuleItem::numPinsInModule()
{
  return m_module->numPinsIn();
}

QString GraphicsModuleItem::nameModule()
{
  return m_module->name();
}

void GraphicsModuleItem::createPinsIn()
{
  int numPinsIn = m_module->numPinsIn();

  if(numPinsIn == 1)
  {
    PinItem *pin = new PinItem(this->pixmap().height()/2,PINTYPE::INPUT,this);
    pinsCreated.append(pin);
  }
  else if(numPinsIn == 2)
  {
    PinItem *pin1 = new PinItem(0,PINTYPE::INPUT,this);
    PinItem *pin2 = new PinItem(pixmap().height(),PINTYPE::INPUT,this);
    pinsCreated.append(pin1);
    pinsCreated.append(pin2);
  }
  else
  {
    int separation = pixmap().height()/numPinsIn;
    int position = 0;
    for(int i = 0;i<numPinsIn;++i)
    {
      PinItem *pin = new PinItem(position,PINTYPE::INPUT,this);
      pinsCreated.append(pin);
      position += separation;
    }
  }
}

void GraphicsModuleItem::createPinsOut()
{
  int numPinsOut = m_module->numPinsOut();

  if( numPinsOut == 1)
  {
    PinItem *pin = new PinItem(this->pixmap().height()/2,PINTYPE::OUTPUT,this);

    pinsCreated.append(pin);
  }
  else if( numPinsOut == 2 )
  {
    PinItem *pin1 = new PinItem(0,PINTYPE::OUTPUT,this);
    PinItem *pin2 = new PinItem(pixmap().height(),PINTYPE::OUTPUT,this);

    pinsCreated.append(pin1);
    pinsCreated.append(pin2);
  }
  else
  {
    int separation = pixmap().height()/numPinsOut;
    int position = 0;
    for(int i = 0;i<numPinsOut;++i)
    {
      PinItem *pin = new PinItem(position,PINTYPE::OUTPUT,this);
      pin->setPos(this->pixmap().width(),0);

      pinsCreated.append(pin);
      position += separation;
    }
  }
}

void GraphicsModuleItem::positionChangedSlot(QPointF p)
{
  if(p != m_module->position())
  {
    this->setPos(p);
    m_module->setPosition(p);
  }
}

QPainterPath GraphicsModuleItem::shape() const
{
  return QGraphicsPixmapItem::shape();
}

void GraphicsModuleItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  QGraphicsPixmapItem::paint(painter,option,widget);
}

QRect GraphicsModuleItem::boundingRect()
{
  return (QRect(0,0,this->pixmap().height(),this->pixmap().width()));
}
