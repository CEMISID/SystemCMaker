#include "GraphicsView.h"
#include <QMimeData>
#include <QDropEvent>
#include <GraphicsModuleItem.h>
#include <QDebug>
#include <QIcon>
#include <PinItem.h>

GraphicsView::GraphicsView(QWidget *parent):
  QGraphicsView(parent)
{
  setAcceptDrops(true);
  m_moduleSelect = false;
  m_pinSelectect = false;
}
GraphicsView::GraphicsView(QGraphicsScene *scene ,QWidget *parent):
  QGraphicsView(scene,parent)
{
  scene = nullptr;
  setAcceptDrops(true);
  m_moduleSelect = false;
  m_pinSelectect = false;
}

GraphicsView::~GraphicsView()
{
  qDeleteAll(m_itemsCreated.begin(), m_itemsCreated.end());
}

void GraphicsView::mouseMoveEvent(QMouseEvent *event)
{
  GraphicsModuleItem * moduleFromCursor = dynamic_cast<GraphicsModuleItem*>
      (this->itemAt(event->pos()));

  if(moduleFromCursor != 0 and m_moduleSelect) //! if some Module is moving, then.
  {
    QPointF point = (QPointF)event->pos();
    moduleFromCursor->setZValue(1);
    moduleFromCursor->setPositionGate(point);
    QList<QGraphicsItem*> childItems=moduleFromCursor->childItems();

    //! Adjust lines connections to items !
    for( auto it : childItems)
    {
      PinItem * pin = dynamic_cast<PinItem*>(it);
      if(pin != 0)
        emit(pin->positionChanged( QPoint(pin->scenePos().x(),
                                          pin->scenePos().y()+pin->getSepartation())));
    }
    event->accept();
  }
  else
    event->ignore();
  QGraphicsView::mouseMoveEvent(event);
}

void GraphicsView::mouseDoubleClickEvent(QMouseEvent *event)
{
  event->ignore();
}

void GraphicsView::mouseReleaseEvent(QMouseEvent *event)
{

  m_moduleSelect = false;
  GraphicsModuleItem * moduleFromCursor = dynamic_cast<GraphicsModuleItem*>
      (this->itemAt(event->pos()));

  PinItem * pinFromCursor = dynamic_cast<PinItem*>(this->itemAt(event->pos()));

  if(pinFromCursor != 0 and m_pinSelectect ) // if mouse is release on one pin item
  {
    //obtain pin sender.
    PinItem * pinSender = dynamic_cast<PinItem*>(this->itemAt( m_intialPointConection));

    //! obtain module sender.
    GraphicsModuleItem * moduleInitialPoint = dynamic_cast<GraphicsModuleItem*>
        (pinSender->parentItem());
    //! obtain module recepter.
    GraphicsModuleItem * moduleEndPoint = dynamic_cast<GraphicsModuleItem*>
        (pinFromCursor->parentItem());

    // if is found module sender and recepter then.
    if(moduleEndPoint != 0 and moduleInitialPoint != 0)
    {
      if(moduleEndPoint != moduleInitialPoint ) // if module sender and recepter are distinct
      {
        //! create connection !

        //! map pos to scene of point start and point end , and crearte connection!
        WireConnection * wire = new WireConnection(mapToScene( m_intialPointConection),
                                                   mapToScene((event->pos())));
        //! ajust wire connection when pin sender changed position.
        connect(pinSender,SIGNAL(positionChanged(QPointF)),wire,SLOT(ajustStart(QPointF)));
        //! ajust wire connection when pin recerpter changed position.
        connect(pinFromCursor,SIGNAL(positionChanged(QPointF)),wire,SLOT(ajustEnd(QPointF)));

        //! add wire to scene. and append to pins created list.
        this->scene()->addItem(wire);
        m_pinsCreated.append(wire);
      }
    }
    m_intialPointConection = QPoint(); // other case point start will be a empty point.(restart)
  }

  if(moduleFromCursor != 0)
    moduleFromCursor->setZValue(-1);

  m_pinSelectect = false;

  QGraphicsView::mouseReleaseEvent(event);
}

void GraphicsView::mousePressEvent(QMouseEvent *event)
{

  PinItem * pinFromCursor = dynamic_cast<PinItem*>(this->itemAt(event->pos()));

  GraphicsModuleItem * moduleFromCursor = dynamic_cast<GraphicsModuleItem*>
      (this->itemAt(event->pos()));

  if(pinFromCursor != 0) // is selected one pin.
  {
    m_intialPointConection = event->pos();
    m_pinSelectect = true;
    event->accept();
  }
  else if(moduleFromCursor != 0)
  {
    m_moduleSelect = true;
    event->accept();
  }
  else
  {
    m_moduleSelect = false;
    m_pinSelectect = false;
    event->ignore();
  }
  QGraphicsView::mousePressEvent(event);
}

void GraphicsView::keyPressEvent(QKeyEvent *event)
{
  switch(event->key())
  {
  case Qt::Key_Delete:
    for(auto it :m_itemsCreated)
    {
      if(it->isSelected())
        this->deleteModuleInView(it);
    }
    break;
  default:
    QGraphicsView::keyPressEvent(event);
  }
}

int GraphicsView::setModuleDataBase(QHash<QString, Module *> *moduleDataBase)
{
  if(moduleDataBase) // if moduleDataBase isn't empty.
  {
   m_modulesDataBase = moduleDataBase;
   return 0;
  }
  return 1;
}

int GraphicsView::addModuleToScene(Module * module)
{
  GraphicsModuleItem * moduleItem = new GraphicsModuleItem(0,module);
  m_itemsCreated.append(moduleItem);

  this->scene()->addItem(moduleItem);

  return 0;
}

void GraphicsView::zoomIn()
{
  this->scale(1.2,1.2);
}

void GraphicsView::zoomOut()
{
  this->scale(0.8,0.8);
}
/*!
 * \brief GraphicsView::deleteModuleInView function , remove item of scene and free
 * memory assignded to it , and makes call to function deleteWireConncetion ,
 * to remove wire connection from scene and free memory assign to it.
 * \param module , module to remove.
 */
void GraphicsView::deleteModuleInView(QGraphicsItem *module)
{
  QList<QGraphicsItem*> pinsOfModule = module->childItems();
  QPointF p;
  PinItem * curr;
  int index = 0;

  for(auto it : pinsOfModule)
  {
    // the coordinates of pin item is respect to module item parent.
    p = it->scenePos(); //map coordinates of pin item to scene coordinates.

    //!is necessary make a little calculus to obtain the correct point in the scene,
    //! this error is because of separation of pin coordinates in module item parent.
    if((curr = dynamic_cast<PinItem*>(it)) == nullptr)
      continue;

    if(curr->getType() == PINTYPE::INPUT)
      p = ( QPoint(p.x(),p.y()+curr->getSepartation()));
    else // PINTYPE == OUTPUT
      p = ( QPoint(p.x(),p.y()+curr->getSepartation()));
    // end of calculus of correct coordinate pos of pin item!

    deleteWireConnection( p);
  }
  this->scene()->removeItem(module);

  emit(moduleDeleteFromViewSignal( (dynamic_cast<GraphicsModuleItem*> (module))->nameModule()));

  for(auto it : m_itemsCreated)
  {
    if(it == module)
      break;
    index++;
  }
  if(m_itemsCreated.isEmpty() or index >= m_itemsCreated.size())
    return;

  delete m_itemsCreated.at(index);
  m_itemsCreated.removeAt(index);

  update();
}

/*!
 * \brief GraphicsView::deleteWireConnection , function to delete a wire connection from
 *the scene , and free memory assing to it. this algoritmh remove and free all wireConnection
 *which point start or point end coindide with some point in WireConnection , by this way ,
 * removove all connections from and to others modules.
 *
 * \param p , point (it can be first or end point!
 */
void GraphicsView::deleteWireConnection(QPointF p)
{
  QList<int> indexs;
  int index=0;
  WireConnection * currWire;

  for(auto it : m_pinsCreated)
  {
    /* is necessary handle certain error or difference bettewen point from user input and
     * points in WireConnections. it error will be -10 px and 10 px around of user input
     * */
    if((it->startPoint().x() - p.x() >= -10 and it->startPoint().x() - p.x() <= 10
       and it->startPoint().y() - p.y() >= -10 and it->startPoint().y() - p.y() <= 10)
       or (it->endPoint().x() - p.x() >= -10 and it->endPoint().x() - p.x() <= 10
           and it->endPoint().y() - p.y() >= -10 and it->endPoint().y() - p.y() <= 10))
    {
      this->scene()->removeItem(it);
      indexs.append(index);
    }
    index++;
  }

  for(auto it : indexs)
  {
    if(m_pinsCreated.isEmpty() or it >= m_pinsCreated.size())
      break;
    currWire = m_pinsCreated.at(it);
    m_pinsCreated.removeAt(it);
    delete currWire;
  }
}

void GraphicsView::dropEvent(QDropEvent *event)
{

  if (event->mimeData()->hasFormat("module->x") )
  {
    QByteArray dataEncoded = event->mimeData()->data("module->x");

    QDataStream stream(&dataEncoded,QIODevice::ReadOnly);
    while (!stream.atEnd())
    {
      QString nameModule;
      QIcon icon;
      stream >> icon >> nameModule;

      //create a new module copy if data base.
      Module *moduleFromMimeTipe = new Module( * m_modulesDataBase->value(nameModule));
      if(moduleFromMimeTipe == 0)
        return;

      moduleFromMimeTipe->setPosition(mapToScene( event->pos()));
      emit(moduleAddToProject(moduleFromMimeTipe)); //Signal to add module to model project.
      addModuleToScene(moduleFromMimeTipe);
    }
  }
  else
    event->ignore();

}

void GraphicsView::dragEnterEvent(QDragEnterEvent *event)
{
  if (event->mimeData()->hasFormat("module->x"))
  {
    event->setDropAction(Qt::MoveAction);
    event->accept();
  }
  else
    event->ignore();
}

void GraphicsView::dragMoveEvent(QDragMoveEvent *event)
{
  if (event->mimeData()->hasFormat("module->x"))
  {
    event->setDropAction(Qt::MoveAction);
    event->accept();
  }
  else
    event->ignore();
}
