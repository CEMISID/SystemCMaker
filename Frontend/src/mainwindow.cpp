#include "mainwindow.h"
#include <QBrush>
#include <QLabel>
#include <QMenuBar>
#include <QDebug>
/*!
 * \brief MainWindow::MainWindow : construct of MainWindow class.
 * \param parent , set parent widget.
 */
MainWindow::MainWindow(QWidget *parent)
  : QMainWindow(parent)
{
  this->setGeometry(0,0,1000,700);


  fileMenu = menuBar()->addMenu(tr("&File"));
  editMenu = menuBar()->addMenu(tr("&Edit"));

  createScene();
  createView();
  createListsViews();
  createModel();

  hLayout = new QHBoxLayout();
  hLayout->addLayout(vLayout);
  hLayout->addWidget(view);

  frame = new QFrame();
  frame->setLayout(hLayout);
  setCentralWidget(frame);

  connect(view,SIGNAL(moduleAddToProject(Module*)),
          this,SLOT(inserModuleInProject(Module*)));
  connect(view,SIGNAL(moduleDeleteFromViewSignal(QString)),
          this,SLOT(deleteModuleFromProject(QString)));
}

MainWindow::~MainWindow()
{
  delete projectView;
  delete baseView;
  delete view;
  delete scene;
  delete frame;
  delete modelBase;
}

void MainWindow::keyPressEvent(QKeyEvent * event)
{
  QList<QGraphicsItem *> items = scene->items();
  switch(event->key())
  {
  case Qt::Key_Escape:
    close();
    break;
  case  Qt::Key_Plus:
    view->zoomIn();
    break;
  case Qt::Key_Minus:
    view->zoomOut();
    break;
  case Qt::Key_Delete:
    for(auto it : items)
    {
      if(it->isSelected())
        view->deleteModuleInView(it);
    }
    break;

  default:
    QMainWindow::keyPressEvent(event);
    break;
  }
}



void MainWindow::addModuleToHash(Module *module)
{
  mapModules.insert(module->name(),module);
}

void MainWindow::removeModuleToHash(Module *module)
{
  mapModules.remove(module->name());
}

void MainWindow::inserModuleInProject(Module *module)
{
  projectModules.append(module);
  modelProject->addModule(module);
}

void MainWindow::deleteModuleFromProject(QString name)
{
  int index = 0;
  for(auto it : projectModules)
  {
    if(it->name() == name)
      break;
    index++;
  }
  if(projectModules.isEmpty() or index >= projectModules.size())
    return;
  if(index == 0 and projectModules.at(0)->name() != name)
    return;

  modelProject->removeModule(name);
  projectModules.removeAt(index);
}


void MainWindow::createScene()
{
  scene = new GraphicsScene(0,0,1000,1000);

}

void MainWindow::createView()
{
  view = new GraphicsView(scene);
  QBrush brushBackGround(Qt::white , Qt::CrossPattern);

  view->setModuleDataBase(&mapModules);
  view->setBackgroundBrush(brushBackGround);
  view->setGeometry(0,0,1000,1000);
}

void MainWindow::createListsViews()
{
  baseView = new QListView();
  projectView = new QListView();

  baseView->setViewMode(QListView::IconMode);
  baseView->setSpacing(10);
  baseView->setIconSize(QSize(70,70));
  baseView->setDragEnabled(true);
  baseView->setMovement(QListView::Snap);
  baseView->setGridSize(QSize(70,70));

  baseView->setMaximumWidth(120);

  modelBase = new ModuleModel();
  modelProject = new ModuleModel();

  baseView->setModel(modelBase);
  projectView->setModel(modelProject);
  projectView->setMaximumWidth(120);

  QLabel * baseText = new QLabel("Base Gates");
  baseText->setFrameStyle(QFrame::Panel | QFrame::Raised);

  QLabel * projectText = new QLabel("Project (*Discar)");
  projectText->setFrameStyle(QFrame::Panel | QFrame::Raised);

  vLayout = new QVBoxLayout();

  vLayout->addWidget(projectText);
  vLayout->addWidget(projectView);
  vLayout->addSpacing(2);
  vLayout->addWidget(baseText);
  vLayout->addWidget(baseView);
}

void MainWindow::createModel()
{
  Module * andGate = new Module("and_gate",":/sources/img/AND_gate.png");
  Module * notGate = new Module("not_gate",":/sources/img/NOT_gate.png");
  Module * orGate = new Module("or_gate",":/sources/img/OR_gate.png");
  Module * xorGate = new Module("xor_gate",":/sources/img/XOR_gate.png");
  Module * someGate = new Module("someGate",":/sources/img/XOR_gate.png");

  notGate->setPinIn(1);
  notGate->setPinOut(1);

  baseModules.append(andGate);
  mapModules.insert(andGate->name(),andGate);


  someGate->setPinIn(2);
  someGate->setPinOut(1);

  baseModules.append(notGate);
  mapModules.insert(notGate->name(),notGate);

  baseModules.append(orGate);
  mapModules.insert(orGate->name(),orGate);

  baseModules.append(xorGate);
  mapModules.insert(xorGate->name(),xorGate);

  baseModules.append(someGate);
  mapModules.insert(someGate->name(),someGate);

  //add modules base to modelBase.
  modelBase->addModule(andGate);
  modelBase->addModule(notGate);
  modelBase->addModule(orGate);
  modelBase->addModule(xorGate);
  modelBase->addModule(someGate);
}
