#include "WireConnection.h"
#include <QDebug>
#include <QPainter>

WireConnection::WireConnection(QGraphicsItem *parent):
QGraphicsPathItem(parent)

{
  setFlags(QGraphicsItem::ItemIsSelectable);
  penWidth = 2;
  color = Qt::black;
}

WireConnection::WireConnection(QPointF _start, QPointF _end, QGraphicsItem *parent):
  QGraphicsPathItem(parent)
{
  setFlags(QGraphicsItem::ItemIsSelectable);
  penWidth = 2;
  color = Qt::black;

  adjust(_start,_end);

}



QPointF WireConnection::startPoint()
{
  return breakPoints.at(0);

}

QPointF WireConnection::endPoint()
{
  return breakPoints.at(3);
}
void WireConnection::adjust(const QPointF &start, const QPointF &end)
{
  CalculatePoints(start,end);
  prepareGeometryChange();
  QPainterPath mpath(start);
  mpath.lineTo(QPointF(breakPoints.at(1)));
  mpath.lineTo(QPointF(breakPoints.at(2)));
  mpath.lineTo(QPointF(breakPoints.at(3)));
  path = mpath;
  setPath(path);
}

void WireConnection::ajustStart(QPointF _start)
{
  QPointF _end = breakPoints.at(3);
  CalculatePoints(_start,_end);
  prepareGeometryChange();
  QPainterPath mpath(_start);
  mpath.lineTo(QPointF(breakPoints.at(1)));
  mpath.lineTo(QPointF(breakPoints.at(2)));
  mpath.lineTo(QPointF(breakPoints.at(3)));
  path = mpath;
  setPath(path);

}

void WireConnection::ajustEnd(QPointF _end)
{
  QPointF start= breakPoints.at(0);
  CalculatePoints(start,_end);
  prepareGeometryChange();
  QPainterPath mpath(start);
  mpath.lineTo(QPointF(breakPoints.at(1)));
  mpath.lineTo(QPointF(breakPoints.at(2)));
  mpath.lineTo(QPointF(breakPoints.at(3)));
  path = mpath;
  setPath(path);

}

void WireConnection::paint(QPainter *painter, const QStyleOptionGraphicsItem *, QWidget *)
{
  painter->setPen(QPen(color, 2, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
  painter->drawPath(path);
}

void WireConnection::mousePressEvent(QGraphicsSceneMouseEvent *)
{
  color = Qt::red;
  update();
  //QGraphicsPathItem::mousePressEvent(event);
}

void WireConnection::CalculatePoints(const QPointF &start, const QPointF &end)
{
  breakPoints.clear();
  if(start.x() < end.x())
  {
    breakPoints.push_back(start);
    int midX =  (end.x()+start.x())/2;
    breakPoints.push_back(QPoint(midX,start.y()));
    breakPoints.push_back(QPoint(midX,end.y()));
    breakPoints.push_back(end);
  }
  else
  {
    breakPoints.push_back(start);
    int midY = (end.y()+start.y())/2;
    breakPoints.push_back(QPoint(start.x(),midY));
    breakPoints.push_back(QPoint(end.x(),midY));
    breakPoints.push_back(end);
  }
}

