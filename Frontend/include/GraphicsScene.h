#ifndef GRAPHICSSCENE_H
#define GRAPHICSSCENE_H

#include <QGraphicsScene>

class GraphicsScene: public QGraphicsScene
{
public:
  GraphicsScene(QObject * parent = 0);
  GraphicsScene(const QRectF & sceneRect, QObject * parent = 0);
  GraphicsScene(qreal x, qreal y, qreal width, qreal height, QObject * parent = 0);

protected:

};

#endif // GRAPHICSSCENE_H
