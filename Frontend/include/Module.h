#ifndef MODULE_H
#define MODULE_H

#include <tuple>
#include <QString>
#include <QPoint>
#include <cstdlib>
#include <QList>

using  ModuleSignalConnect = std::tuple<QString,QString,QString>;

enum class ModuleType {
  AND_GATE , OR_GATE , XOR_GATE , NOT_GATE , NAND_GATE , MODULE_GATE
};

class Module
{
public:
  Module();
  Module(const QString & name = "module",
         const QString & imgDir = "NONE" ,
         const ModuleType & type = ModuleType::MODULE_GATE ,
         const QList<Module*> & modulesList = QList<Module*>(),
         const QList<ModuleSignalConnect> & signalsList = QList<ModuleSignalConnect>(),
         const QPointF & modulePos = QPointF(0,0),
         const size_t & width = 100,
         const size_t & height = 100,
         const size_t & pinsIn = 2,
         const size_t & pinsOut = 1,
         const QString & dataFlow = "bool");

  Module( Module & module);


  inline QString name        () const { return m_moduleName;     }
  inline QString imgDir      () const { return m_moduleImgDir;   }
  inline QString dataFlowType() const { return m_dataFlowType;   }
  inline QPointF position    () const { return m_positionModule; }
  inline size_t  height      () const { return m_height;         }
  inline size_t  width       () const { return m_width ;         }
  inline size_t  numPinsIn   () const { return m_numPinIn;       }
  inline size_t  numPinsOut  () const { return m_numPinOut;      }

  inline ModuleType                        circuitType             ()       {return m_circuitType             ;}
  inline const QList<Module*>&             modulesInternalList     () const {return m_modulesInternalList     ;}
  inline const QList<ModuleSignalConnect>& moduleSignalInternalList() const {return m_moduleSignalInternalList;}

  int addPinIn(int num = 1);
  int addPinIn(const QString & nameInternaModule , int num = 1);
  int addPinOut(int num = 1);
  int addPinOut(const QString & nameInternaModule , int num = 1);

  int setPinIn(int num = 1);
  int setPinIn(const QString & nameInternaModule , int num = 1);
  int setPinOut(int num = 1);
  int setPinOut(const QString & nameInternaModule , int num = 1);

  int addModule(Module * module);
  int addSignalModuleInternal( const ModuleSignalConnect & signal );
  int addSignalModuleInternal(const QString & fst , const QString & snd , const QString & name);
  int setPosition(const QPointF & newPoint);
  int setHeight( size_t height);
  int setWidth( size_t width);

private:
  ModuleType m_circuitType;
  QString m_moduleName;
  QString m_moduleImgDir;
  QString m_dataFlowType;
  QList<Module*> m_modulesInternalList;
  QList<ModuleSignalConnect> m_moduleSignalInternalList;
  QPointF m_positionModule;
  size_t m_height;
  size_t m_width;
  size_t m_numPinIn;
  size_t m_numPinOut;
};

#endif // MODULE_H
