/* this class allows the representation of a object of the TDA Module, to be perfoms on a
 * painter device, is part of de MVC programming aproach , provide a implementation
 * to allow drag and drop feactures to de models .
 *
 * this TDA have a private member that is a list of pointer to Module objects ,
 *  so , this model work on a extern data and avoid copy data innecesary.
*/

#ifndef GATEMODEL_H
#define GATEMODEL_H

#include <QAbstractListModel>
#include <QList>
#include <QPixmap>
#include <QMimeData>

#include "Module.h"
/*!
 * \brief The GateModel class : Class based on MVC approach. to handle the paint of
 * modules or gate on canvas.
 */

class ModuleModel : public QAbstractListModel
{
  Q_OBJECT

public:
/* constructs of the class */

  /*!
   * \brief GateModel default constructer
   * \param parent
   */
  explicit ModuleModel(QObject *parent = 0);


  /*!
   * \brief GateModel : parametrizer constructer
   * \param modules : reference to modules base.
   * \param parent : parent QObject.
   */
  ModuleModel(QList<Module *> &modules, QObject * parent = 0);

  ~ModuleModel(){};


  /*!
    * \brief rowCount : Returns the number of rows under the given parent.
    * \param parent : parent of model, by defect create a model empty.
    * \return int , the number of rows (or gates in model) in the model.
    */
  int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE ;

  /*
   * \brief this function allows recover data from model , it information depend of role
   * , if the role is Qt::DecorationRole , it function returns a QIcon ,
   * if the role is Qt::UserRole ir return the name of gate or module , and it allow map the
   * gate onto date base togheter a QHash for example.
   * \param index m index onto model,
   * \param role , given role return a underlyging type.
   * \return a QVariant whit gate's information based in role param.
  */
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
  Qt::ItemFlags flags(const QModelIndex &index) const Q_DECL_OVERRIDE;
  QMimeData *mimeData(const QModelIndexList &indexes) const Q_DECL_OVERRIDE;
  QStringList mimeTypes() const Q_DECL_OVERRIDE;

  int addModule(Module *module);
  int addModule(QList<Module*> & modules);

  int removeModule(QString);
  int removeModule(QList<QString>);
private:
  QList<Module*> m_modules;

};

#endif // GATEMODEL_H
