#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QList>
#include <QListView>
#include <QBoxLayout>
#include <QFrame>
#include <QHash>
#include <QKeyEvent>

#include "ModuleModel.h"
#include "GraphicsView.h"
#include "GraphicsScene.h"
#include "Module.h"

/*!
 * \brief The MainWindow class : main class to handle
 * the windows's program
 */
class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  MainWindow(QWidget *parent = 0);
  ~MainWindow();
protected:
  void keyPressEvent(QKeyEvent * event);


  //! Qt private slots begin
private slots:
  void addModuleToHash(Module* module);
  void removeModuleToHash(Module*module);
  void inserModuleInProject(Module *module);
  void deleteModuleFromProject(QString);

  //! Qt private slots end

private:

  QFrame *frame;
  GraphicsView *view;
  GraphicsScene *scene;
  QListView *baseView;
  QListView *projectView;
  QVBoxLayout *vLayout;
  QHBoxLayout *hLayout;

  QList<Module*> baseModules;
  QList<Module*> projectModules;

  QHash<QString,Module*> mapModules;

  ModuleModel *modelBase;
  ModuleModel *modelProject;

  //! menus beging
  QMenu *fileMenu;
  QMenu *editMenu;
  QToolBar *fileToolBar;
  QToolBar *editToolBar;
  //! menus end

  void createScene();
  void createView();
  void createListsViews();
  void createSplitter();
  void createModel();
};

#endif // MAINWINDOW_H
