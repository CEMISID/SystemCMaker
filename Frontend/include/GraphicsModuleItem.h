#ifndef GRAPHICSMODULEITEM_H
#define GRAPHICSMODULEITEM_H

#include <QGraphicsPixmapItem>
#include <QLine>
#include <QtCore/QObject>

#include "PinItem.h"
#include "Module.h"
class GraphicsModuleItem: public QObject, public QGraphicsPixmapItem
{
  Q_OBJECT
public:
  GraphicsModuleItem(QGraphicsItem * parent = 0,Module *module= nullptr);
  GraphicsModuleItem(QGraphicsItem * parent = 0);


  int setModuleItem(Module *module = nullptr);
  void setPositionGate(QPointF);
  int numPinsInModule();
  QString nameModule();

signals:
  void positionModuleChanged(QPointF);

protected:
  QPainterPath shape() const;
  void	paint(QPainter * painter, const QStyleOptionGraphicsItem * option, QWidget * widget = 0);
  QRect boundingRect();

public slots:
  void positionChangedSlot(QPointF);

private:
  QList<PinItem*> pinsCreated;
  Module *m_module;

  void createPinsIn();
  void createPinsOut();

};

#endif // GRAPHICSMODULEITEM_H
